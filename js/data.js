var Data = {
	from: 0,
    size: 9,
    data: [],
    $slider : $('.slide-section-3'),
	init: function(){
		var searchParams = {
			index: 'products',
            type: 'dishrestaurant',
            from: 0,
            size: 20,
            body: {
            	query: {
            		bool: {
            			"must": [
            			{ "match": { "merchantId": "2b4795d4-e39c-418e-8292-6b6aa2d66f92" } }
            			]
            		}
            	}
            }
        };
        client.search(searchParams, function(error, data){
        	$.each(data.hits.hits, function(i, val){
        		Data.data.push(val._source);
        	});
        	console.log(Data.data);
            var html = Data.buildSlide(Data.data);
            // $('.slider-3 .slick-track').append(html);
            
        })
    },

   	buildSlide(data){
   		var build = '';
        var imageUrl = '';
        // imageUrl = Configuration.imageRoot + value.path + cropSize;
        $.each(data, function(i, val){
            $.each(val.images,function (index, value) {
                        if (value.isFeatured === true) {
                            imageUrl = Configuration.imageRoot + value.path;
                            return false;
                        }
                    });
            build+='   <div class="item-slider main-banner col-md-4 col-xs-6 col-12">';
            build+='       <img src="'+imageUrl+'">';
            build+='       <div class="content-slider-sc3 text-center">';
            build+='           <h4><b>'+data.name+'</b></h4>';
            build+='           <br>';
            build+='           <div></div>';
            build+='       </div>';
            build+='       <div class="overlay">';
            build+='           <div class="text-overlay">';
            build+='               Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquam id est laborum.';
            build+='           </div>';
            build+='       </div>';
            build+='   </div>';
        });
        return build;
   	},


}